const gulp = require('gulp');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const wait = require('gulp-wait');
const glob = require('glob');
const path = require('path');

const paths = {
  styles: {
    src: 'src/sass/**/',
    ext: 'src/sass/**/*.{css,scss,sass}',
    dest: 'styles/css'
  }
}

// Quick/efficient way to get the unique values from a array.
function uniqValuesArray(a) {
  var seen = {};
  var out = [];
  var len = a.length;
  var j = 0;
  for(var i = 0; i < len; i++) {
       var item = a[i];
       if(seen[item] !== 1) {
             seen[item] = 1;
             out[j++] = item;
       }
  }
  return out;
}
function getCurrentTimestamp() {
  const date = new Date();

  const hours = String(date.getHours()).padStart(2, "0");
  const minutes = String(date.getMinutes()).padStart(2, "0");
  const seconds = String(date.getSeconds()).padStart(2, "0")
  
  return `\x1b[35m[${hours}:${minutes}:${seconds}]\x1b[0m`
}

let filteredFiles = [];

gulp.task('getFiles', function (done) {
  glob(paths.styles.ext, function (er, files) {
    filteredFiles = uniqValuesArray([...files.map((file) => path.basename(file))]);
    done();
  })
})

gulp.task('sass', function (done) {
  filteredFiles.forEach((file) => {
    const fileName = file;

    gulp.src(['src/sass/utils/_mixin.scss', 'src/sass/utils/_vars.scss',paths.styles.src + fileName])
      .pipe(concat(fileName))
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest(paths.styles.dest))
  })
  done()
});

gulp.task('run', gulp.series('getFiles', 'sass'))

gulp.task('watch', function () {
  gulp.watch(paths.styles.ext)
    .on("change", function (fileName) {
      fileName = path.basename(fileName);
      
      if (fileName.includes('.css')) {
        gulp.src(paths.styles.src + fileName)
        .pipe(concat(fileName))
        .pipe(gulp.dest(paths.styles.dest));
      } else {
        gulp.src(['src/sass/utils/_mixin.scss', 'src/sass/utils/_vars.scss',paths.styles.src + fileName])
        .pipe(concat(fileName))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(paths.styles.dest))
      }

      console.log(getCurrentTimestamp() + " File: \x1b[31m" + fileName + '\x1b[0m builded.')
    })
})

gulp.task('default', gulp.series('run', 'watch'))