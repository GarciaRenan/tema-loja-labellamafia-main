import React, { useState } from 'react'
import useProduct from 'vtex.product-context/useProduct'
import { FormattedMessage } from 'react-intl'
import { useCssHandles } from 'vtex.css-handles'

import ScopedStyledComponent from './components/ScopedStyledComponent'
import { Open, Overlay, Content, ModalHeader, Title, Close } from './styles/SizeTable';

const CSS_HANDLES = [
  'sizeTable',
  'sizeTableActionOpen',
  'sizeTableOverlay',
  'sizeTableModalHeader',
  'sizeTableModalTitle',
  'sizeTableActionClose',
  'sizeTableModal',
  'sizeTableModalImage',
]

const SizeTable: StorefrontFunctionComponent<SizeTableProps> = () => {
  const handles = useCssHandles(CSS_HANDLES)
  const { product } = useProduct()
  const [isOpen, setIsOpen] = useState(false)
  let openClass = isOpen ? 'active' : ''

  function getImagePath() {
    let imageSrc = `/arquivos/st-default.png`

    if (product.hasOwnProperty('categoryTree')) {
      let departament = product.categoryTree[0].name

      departament = departament.toLowerCase().replace(' ', '-')
      imageSrc = `/arquivos/st-${departament}.png`
    }

    return imageSrc
  }

  return (
    <ScopedStyledComponent COMPONENT_SCOPED_NAME="sizeTable">
      <div className={`${handles.sizeTable} mb5`}>
        <Open
          className={`${handles.sizeTableActionOpen} f5`}
          onClick={() => setIsOpen(!isOpen)}
        >
          <FormattedMessage id="store/sizeTable.actionOpen" />
        </Open>
        <div>
          <Overlay
            className={`${handles.sizeTableOverlay} ${openClass}`}
            onClick={() => setIsOpen(!isOpen)}
          />
          <Content className={`${openClass} ${handles.sizeTableModal}`}>
            <ModalHeader className={handles.sizeTableModalHeader}>
              <Title className={`${handles.sizeTableModalTitle} f-3`}>
                <FormattedMessage id="store/sizeTable.title" />
              </Title>
              <Close
                className={`${handles.sizeTableActionClose}`}
                onClick={() => setIsOpen(!isOpen)}
              ></Close>
            </ModalHeader>

            <div className={`${handles.sizeTableModalImage}`}>
              <img src={getImagePath()} alt="" />
            </div>
          </Content>
        </div>
      </div>
    </ScopedStyledComponent>
  )
}

interface SizeTableProps {}

export default SizeTable
