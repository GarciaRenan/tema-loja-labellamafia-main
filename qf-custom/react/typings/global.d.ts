export interface ToggleSearchProps {
  containerWidth: number
  buttonSize: number
  buttonIcon: string
  buttonCloseIcon: string
}

export interface CategoryDescription {
  contentId: number
}

export interface SimilarProductsProps {
  imageWidth: number
  imageHeight: number
  textTitle: string
  msgError: string
  msgLoading: string
}

export interface ShelfSimilarProductsProps {
  maxQtyVisible: number
}

export interface StockFlagProps {
  unavailableText: string
}

export interface InstitucionalNav {
  links: InstuticionalLink[]
}

export interface InstuticionalLink {
  href: string
  label: string
}

declare global {
  interface Window extends Window {
    __APP_ID__: string
    __ERROR__: any
    __hasPortals__: boolean
    __hostname__: string
    __pathname__: string
    __provideRuntime: (
      runtime: RenderContext | null,
      messages: Record<string, string>,
      shouldUpdateRuntime: boolean,
      setMessages: (messages: RenderRuntime['messages']) => void
    ) => Promise<void>
    __RENDER_8_COMPONENTS__: ComponentsRegistry
    __RENDER_8_HOT__: HotEmitterRegistry
    __RENDER_8_RUNTIME__: RuntimeExports
    __RENDER_8_SESSION__: RenderSession
    __REQUEST_ID__: string
    __RUNTIME__: RenderRuntime
    __RUNTIME_EXTENSIONS__: RenderRuntime['extensions']
    __RUNTIME_QUERYDATA__: RenderRuntime['queryData']
    __STATE__: NormalizedCacheObject
    __DOM_READY__?: boolean
    __ASYNC_SCRIPTS_READY__?: boolean
    __CRITICAL__UNCRITICAL_APPLIED__: Promise<void> | undefined
    __CRITICAL__RAISE_UNCRITICAL_EVENT__: () => void | null
    browserHistory: History
    flags: Record<string, boolean>
    hrtime: NodeJS.Process['hrtime']
    Intl: any
    IntlPolyfill: any
    myvtexSSE: any
    ReactAMPHTML: any
    ReactAMPHTMLHelpers: any
    ReactIntlLocaleData: any
    ReactIntl: any
    IntlPluralRules: any
    IntlRelativeTimeFormat: any
    rendered: Promise<RenderedSuccess> | RenderedFailure
    requestIdleCallback: (callback: (...args) => any | void) => number
    ReactApollo: any
  }

  namespace NodeJS {
    interface Global extends Window {
      myvtexSSE: any
    }
  }

  interface NodeModule {
    hot: any
  }
}
