import styled from 'styled-components'

export const Open = styled.button`
  padding: 0;
  outline: none;
  background: transparent;
  text-transform: uppercase;
  text-decoration: underline;
  line-height: 14px;
  cursor: pointer;
  transition: color 0.2s linear;
  border: none;
  font-size: 14px;
  color: #000;
  letter-spacing: 0.87px;

  &:hover {
    color: #707070;
  }
`

export const Overlay = styled.div`
  display: none;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 999;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);

  &.active {
    display: block;
  }
`

export const Content = styled.div`
  display: none;
  position: fixed;
  top: 50%;
  left: 50%;
  z-index: 9999;
  transform: translate(-50%, -50%);
  width: 95%;
  max-width: 460px;
  padding: 12px 16px;
  background: #ffffff;

  &.active {
    display: block;
  }
`

export const ModalHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #525252;
  padding-bottom: 12px;
  margin-bottom: 12px;
`

export const Title = styled.div`
  text-transform: uppercase;
  font-size: 14px;
  font-weight: 500;
  color: #272727;
`

export const Close = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  width: 20px;
  height: 20px;
  padding: 0;
  border: none;
  outline: none;
  background: transparent;
  cursor: pointer;

  &::before,
  &::after {
    content: '';
    position: absolute;
    width: 20px;
    height: 2px;
    background: #272727;
  }

  &::before {
    transform: rotate(45deg);
  }

  &::after {
    transform: rotate(-45deg);
  }
`
