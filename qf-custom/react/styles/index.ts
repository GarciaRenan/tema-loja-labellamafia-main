import styled, { css } from 'styled-components'

interface ISearchContainer {
  width: number
  isOpened: boolean
}

interface IButton {
  size: number
  icon: string
  closeIcon: string
  isOpened: boolean
}

export const Wrapper = styled.div`
  position: relative;
`

export const SearchContainer = styled.div<ISearchContainer>`
  position: absolute;
  top: 50%;
  right: calc(560% + 55px);
  transform: translateY(-50%);
  max-width: 1000px;
  background: transparent;
  display: none;
  width: 44vw;

  @media only screen and (max-width: 1024px) {
    background: white;
    position: fixed;
    top: 103px;
    right: 0;
    width: 100%;
    max-width: 100%;
  }

  ${(props) =>
    props.isOpened
      ? css`
          display: block;
        `
      : css`
          display: none;
        `}

  ${(props) =>
    props.width
      ? css`
          width: ${props.width}vw;
        `
      : css`
          width: 36vw;
        `}
`

export const Button = styled.button<IButton>`
  display: block;
  padding: 0;
  border: none;
  outline: none;
  background-color: transparent;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  cursor: pointer;
  margin: 0 5px;
  min-width: 19px;
  height: 19px;
  background-image: url(assets/svgs/search-icon.svg);


  ${(props) =>
    props.size
      ? css`
          min-width: ${props.size}px;
          height: ${props.size}px;
        `
      : css`
          min-width: 20px;
          height: 20px;
        `}

  ${(props) =>
    props.icon &&
    css`
      background-image: url(${props.icon});
    `}

  ${(props) =>
    props.isOpened &&
    css`
      background-image: url(${props.closeIcon});
      height: 25px;
    `}
`

export const StockFlagContainer = styled.div`
  box-sizing: border-box;
  min-width: 64px;
  padding: 4px;
  background: #272727;
`

export const StockFlagText = styled.span`
  display: block;
  text-transform: uppercase;
  font-size: 12px;
  color: #ffffff;

  @media only screen and (max-width: 1025px) {
    font-size: 10px;
  }
`
