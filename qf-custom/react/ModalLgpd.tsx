import React, { useState, useEffect } from 'react';
import style from './styles/modal.css';

import CookieService from './utils/CookieService';

function handleBtn(lgpdCookie: any, setActive: any) {
  if (!lgpdCookie) {
    CookieService.setCookie('lgpdChoose', 'true', 1);
    setActive('');
  }
}

const ModaLgpd: StorefrontFunctionComponent<ModalProps> = () => {
  const [lgpdCookie, setLgpdCookie]: any = useState('');
  const [active, setActive] = useState('');

  useEffect(() => {
    if (document !== undefined) {
      const lgpdCookieValue: any = CookieService.getCookie('lgpdChoose');
      const activeValue = lgpdCookieValue?.length ? '' : style.modalLgpdActive;

      setLgpdCookie(lgpdCookieValue);
      setActive(activeValue);
    }
  }, [active]);

  return (
    <div className={`${style.modalLgpd} ${active}`} style={{ display: 'none' }}>
      <p className={style.modalLgpdText}>
        Em conformidade com os artigos 7º, inciso I e 8º, § 1º da Lei nº
        13.709/18 - Lei de Proteção de Dados Pessoais <b>(LGPD)</b>, as partes
        consentem, de forma livre a La Chocolê a utilizar os seus dados pessoais
        necessários para a finalidade de envio de newsletters informativas e
        promocionais da marca.
      </p>
      <button
        className={style.modalLgpdBtn}
        onClick={() => handleBtn(lgpdCookie, setActive)}
      >
        OK
      </button>
    </div>
  );
};

interface ModalProps {}

export default ModaLgpd;
