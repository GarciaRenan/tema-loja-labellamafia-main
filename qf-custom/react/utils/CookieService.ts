// function cookieDate(d:any) {
//   function d2(n:any) { return n < 10 ? '0' + n : n; }
//   var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
//     months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

//   return '' +
//     days[d.getUTCDay()] + ', ' +
//     d2(d.getUTCDate()) + '-' +
//     months[d.getUTCMonth()] + '-' +
//     d.getUTCFullYear() + ' ' +
//     d2(d.getUTCHours()) + ':' +
//     d2(d.getUTCMinutes()) + ':' +
//     d2(d.getUTCSeconds()) + ' GMT';
// }

const setCookie = (cookieKey:any , cookieValue:any, expirationDays:any) => {
  let expiryDate = '';

  if (expirationDays) {
    // const date:any = new Date();

    // date.setTime(`${date.getTime()}${(expirationDays || 30 * 24 * 60 * 60 * 1000)}`);
    // console.log(date.toUTCString());

    expiryDate = `; max-age= ${60 * 60 * 24 * expirationDays}`;
  }

  document.cookie = `${cookieKey}=${cookieValue || ''}${expiryDate}; path=/`;
  console.log(expiryDate);
}

const getCookie = (cookieKey:any) => {
  let cookieName = `${cookieKey}=`;

  let cookieArray = document.cookie.split(';');

  for (let cookie of cookieArray) {

    while (cookie.charAt(0) == ' ') {
          cookie = cookie.substring(1, cookie.length);
      }

    if (cookie.indexOf(cookieName) == 0) {
          return cookie.substring(cookieName.length, cookie.length);
      }
  }
  
  return undefined;
}

export default { setCookie, getCookie };