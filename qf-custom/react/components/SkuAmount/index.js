import React, { useEffect } from 'react';
import { useProduct } from 'vtex.product-context';

function index() {
  const productContext = useProduct(),
    items = productContext.product.items,
    widthScreen = window.innerWidth;

  useEffect(() => {
    const selectSize = document?.querySelectorAll(
      '.vtex-store-components-3-x-skuSelectorItemTextValue--qf-custom-sku-selector',
    );
    const selectSizeMobile = document?.querySelectorAll(
      '.vtex-store-components-3-x-skuSelectorOptionsList .vtex-dropdown__caption .h-100.flex.items-center',
    );
    const selectSizeMobileOption = document?.querySelectorAll(
      '.vtex-store-components-3-x-skuSelectorOptionsList .vtex-styleguide-9-x-container option',
    );

    items.map(item => {
      let qtt = item.sellers[0].commertialOffer.AvailableQuantity;
      let variations = item.variations[0].values[0];

      if (widthScreen > 1025) {
        selectSize.forEach(item => {
          if (item.innerHTML == variations) {
            item.innerHTML = `${variations}<span class="variations">(${qtt})</span>`;
          }
        });
      } else {
        selectSizeMobile.forEach(item => {
          if (item.innerHTML == variations) {
            item.innerHTML = `${variations}<span class="variations">(${qtt})</span>`;
          }
        });
        selectSizeMobileOption.forEach(item => {
          if (item.innerHTML == variations) {
            item.innerHTML = `${variations}<span class="variations">(${qtt})</span>`;
          }
        });
      }
    });
  }, []);
  return <></>;
}

export default index;
