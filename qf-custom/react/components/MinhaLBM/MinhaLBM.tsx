import React, { useState, useEffect } from 'react'

import style from './MinhaLBM.css';

const MinhaLBM = () => {

  // const [products, setProducts] = useState([]) as any;
  const [productList, setProductList] = useState([]) as any;
  const [detailsVendor, setdetailsVendor] = useState([]) as any;


  useEffect(() => {

    const { hash } = window.location;
    const params = (new URL(document.location.href)).searchParams
    const namestore = params.get('utm_campaign');

    if (!hash && !namestore) window.location.replace('https://devteste--labellamafia.myvtex.com');

    if (hash) window.location.replace('https://devteste--labellamafia.myvtex.com/loja?utm_source=mais&utm_medium=minhalojapgm&utm_campaign=' + hash.replace('#', ''));

    fetch('https://apilabellamafia.mais.com.br/api/Platform/FindDataByDomain/' + namestore, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'x-server-origin': 'loja.labellamafia.com.br',
        Accept: 'application/json'
      }
    })
      .then((response) => response.json())
      .then((endResponse) => {

        if (endResponse == '[]') {
          window.location.replace('https://devteste--labellamafia.myvtex.com');
        } else {
          setdetailsVendor(endResponse);

          fetch('https://apilabellamafia.mais.com.br/api/Platform/FindProductsStoreByDomain/' + namestore, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              'x-server-origin': 'loja.labellamafia.com.br',
              Accept: 'application/json'
            }
          })
            .then((response) => response.json())
            .then((endResponse) => {
              setProductList(endResponse);
            });
        }
      });

  }, [])

  return (
    <>
      <div className={style.containerbkg}>
        <div className={style.container}>
          <div id="top">

            {detailsVendor.map((detail: any) => {
              return (
                <div className={style.cntAll}>
                  <div className={style.banner}>
                    <img src={detail.url} alt="" />
                  </div>
                  <div className={style.presentation}>
                    <div className={style.profileImage}>
                      <img src={detail.image} alt="" />
                    </div>
                    <div className={style.description}>
                      <div className={style.title}>
                        {detail.name}
                      </div>
                      <div className={style.content}>
                        {detail.description}
                      </div>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>

          <div className={style.products}>
            {productList ?
              <>
                {productList.map((product: productMais) => {
                  const { items } = product;
                  const { images } = items[0];
                  const { name } = items[0];
                  const { sellers } = items[0];
                  let priceteste = (parseFloat(items[0].sellers[0].commertialOffer.price) / 6)-0.005;
                  const price = priceteste.toLocaleString('pt-br', {
                    style: 'currency',
                    currency: 'BRL',
                  });
                  const sellerBR = parseFloat(
                    sellers[0].commertialOffer.price
                  ).toLocaleString('pt-br', {
                    style: 'currency',
                    currency: 'BRL',
                  });
                  const nameCard = name.length > 40 ? name.slice(0, 37) + '...' : name;

                  return (
                    <div className={style.cardBox} key={`${items.productId}`}>
                      <a href={`https://devteste--labellamafia.myvtex.com/${product.linkText}/p`} >
                        <div className={style.imgProduct} key={`${items.productId}`}>
                            <img className={style.img} src={images[0].imageUrl && images[0].imageUrl !== '0'
                              ? images[0].imageUrl
                              : 'https://labellamafia.vteximg.com.br/arquivos/imagemDefaulDSM.png'
                            } alt={nameCard} key={`${items.productId}`} />
                        </div>
                        <div className={style.descriptionProduct} key={`${items.productId}`}>
                          <div className={style.offer}>
                            {/* <div className={style.superOffer}>SUPER OFERTA</div> */}
                            <div className={style.title}>
                              {nameCard}
                            </div>
                          </div>
                          <div className={style.price}>
                            <div className={style.value}>
                              {sellerBR}
                            </div>
                            <div>
                              <div className={style.valueParcel}>
                                ou em 6x de {price}
                              </div>
                            </div>
                            {/* <div className={style.discount}>14% OFF</div> */}
                          </div>
                          {/* <div className={style.add}>
                            <p className={style.addButon}>VER MAIS</p>
                          </div> */}
                        </div>
                      </a>
                    </div>
                  );
                })}
              </>
              :
              ''
            }
          </div>
        </div>
      </div>


    </>
  )
}

export default MinhaLBM;