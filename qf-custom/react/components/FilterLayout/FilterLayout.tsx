import React, { useState, useEffect } from "react";
import styles from "../../styles/filter-layout.css";

const FilterLayout: StorefrontFunctionComponent<IProps> = (props) => {
    const [filterOpen, setFilterOpen] = useState(true);

  const components = props?.components ? props?.components : {};

  const listComponents: IComponents = {
    orderBy: '',
    subcategories: '',
    filterNavigator: '',
    filterSwitcher: '',
    breadcrumb: '',
  };

  props.children.map((child: IChild) => {
    const id = child.props.id;

        if (id === components.orderBy) listComponents.orderBy = child;
        if (id === components.subcategories)
            listComponents.subcategories = child;
        if (id === components.filterNavigator)
            listComponents.filterNavigator = child;
        if (id === components.filterSwitcher)
            listComponents.filterSwitcher = child;
        if (id === components.breadcrumb) listComponents.breadcrumb = child;
    });
    
    useEffect(() => {
        setTimeout(() => {

        const closeModal = document.querySelector('.close-modal');
        
        if(closeModal) {
                closeModal.addEventListener('click', () => {
                    setFilterOpen(false);
                })
        }
        }, 2000)   

    }, []); 

    return (
        <section 
            className={`${styles["filterLayout"]} 
            ${filterOpen ? styles["filterLayout-opened"] : ''}`}
        >
            <div className={styles["filterLayout--subcategoriesContainer"]}>
                {listComponents?.subcategories}
            </div>
            <div className={styles["filterLayout--breadcrumb"]}>
                {listComponents?.breadcrumb}
            </div>
            <div
                className={` ${
                    filterOpen ? styles["filterLayout--filterOpen"] : ""
                } ${styles["filterLayout--filter"]} `}
            >
                {listComponents?.filterNavigator}
            </div>
            <div className={` ${
                    filterOpen ? styles["fiterLayoutOverlay--open"] : ""
                }`}
            ></div>
        </section>
    );
};

interface IProps {
  components: IComponents;
  children: Array<IChild>;
}

interface IComponents {
  orderBy?: string | IChild;
  subcategories?: string | IChild;
  filterNavigator?: string | IChild;
  filterSwitcher?: string | IChild;
  breadcrumb?: string | IChild;
}

interface IChild {
  props: IChildProps;
}

interface IChildProps {
  id: string;
}

export default FilterLayout;
