import React, { useEffect } from 'react';

function debounce(method: any, delay: any) {
  clearTimeout(method._tId);
  method._tId = setTimeout(function() {
    method();
  }, delay);
}

function desktopScroll() {
  if (window.pageYOffset < 100) {
    (document as any).querySelector('.vtex-minicart-2-x-drawer').style.top =
      '102px';
  } else {
    (document as any).querySelector(
      '.vtex-minicart-2-x-drawer',
    ).style.top = `${(document as any).querySelector(
      '.vtex-minicart-2-x-minicartContainer',
    ).offsetTop + 40}px`;
  }
}

const FixedMinicart: StorefrontFunctionComponent = () => {
  useEffect(() => {
    if (typeof window != undefined && window.innerWidth < 1020) {
      const openMenuInterval = setInterval(() => {
        const openMenu = document.querySelector(
          '.vtex-store-drawer-0-x-openIconContainer--menu-mobile',
        );

        if (openMenu) {
          openMenu.addEventListener('click', () => {
            const minicartOverlay: any = document.querySelector(
              '.vtex-minicart-2-x-overlay',
            );

            setTimeout(() => {
              const closeMenu: any = document.querySelector(
                '.vtex-store-drawer-0-x-closeIconButton--menu-close-button',
              );

              if (closeMenu) {
                closeMenu.addEventListener('click', () => {
                  setTimeout(() => {
                    const subMenuCloseBtn: any = document.querySelector(
                      '.vtex-store-drawer-0-x-opened--secondary-tab .vtex-store-drawer-0-x-closeIconButton',
                    );
                    minicartOverlay.classList.remove('menu-opened');

                    if (subMenuCloseBtn) {
                      subMenuCloseBtn.click();
                    }
                  }, 100);
                });
              }
            }, 500);

            if (minicartOverlay) {
              minicartOverlay.classList.add('menu-opened');
            }
          });

          clearInterval(openMenuInterval);
        }
      }, 500);
    }
  }, []);

  if (typeof window != undefined) {
    (window as any).addEventListener(
      'load',
      () => {
        (document as any).querySelector(
          '.vtex-minicart-2-x-drawer',
        ).style.right = `${window.innerWidth -
          (document as any).querySelector(
            '.vtex-minicart-2-x-minicartWrapperContainer',
          ).offsetLeft -
          (document as any).querySelector(
            '.vtex-minicart-2-x-minicartWrapperContainer',
          ).offsetWidth}px`;
      },
      true,
    );
  }

  if (typeof window != undefined && window.innerWidth > 1026) {
    (window as any).addEventListener(
      'resize',
      () => {
        (document as any).querySelector(
          '.vtex-minicart-2-x-drawer',
        ).style.right = `${window.innerWidth -
          (document as any).querySelector(
            '.vtex-minicart-2-x-minicartWrapperContainer',
          ).offsetLeft -
          (document as any).querySelector(
            '.vtex-minicart-2-x-minicartWrapperContainer',
          ).offsetWidth}px`;
      },
      true,
    );
  }

  if (typeof window != undefined && window.innerWidth > 1026) {
    (window as any).addEventListener(
      'scroll',
      () => {
        debounce(desktopScroll, 300);
      },
      true,
    );
  }

  return <></>;
};

export default FixedMinicart;
