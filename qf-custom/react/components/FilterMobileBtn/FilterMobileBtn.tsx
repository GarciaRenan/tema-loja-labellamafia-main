import React from "react";
import { useSearchPage } from 'vtex.search-page-context/SearchPageContext'

import styles from "../../styles/filter-mobile-btn.css";

const FilterMobileBtn: StorefrontFunctionComponent = () => {
    const {
        searchQuery
      } = useSearchPage()

    const searchTerm = searchQuery.data?.searchMetadata?.titleTag;
    const quantityProducts = searchQuery?.recordsFiltered;


    const handleBtnClick = () => {
        const filterMobile = document
            .querySelector('.vtex-flex-layout-0-x-flexColChild--filterMobileCol .labellamafia-custom-search-result-0-x-filters--layout');
        const overlayMobile = document
        .querySelector('.vtex-flex-layout-0-x-flexColChild--filterMobileCol .labellamafia-custom-search-result-0-x-queryOverlay');


        if (filterMobile) {
            filterMobile.classList.toggle('open');
            overlayMobile?.classList.toggle('open');
        }
    };  

    return (
        <div className={styles.filterMobileContainer}>
            <p className={styles.queryTitle}>
            {searchTerm}

            <span className={styles.queryQuantity}>[{quantityProducts}]</span>
          </p>

            <button 
                onClick={handleBtnClick}
                className={styles["filterMobileBtn"]}
            >
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <g id="Icons_Multimedia_sliders-v-alt" data-name="Icons/Multimedia/sliders-v-alt" transform="translate(24) rotate(90)">
                        <path id="sliders-v-alt" d="M16.977,20a1,1,0,0,1-1-1V11.82a3,3,0,0,1,0-5.639V1a1,1,0,1,1,2,0V6.18a3,3,0,0,1,0,5.639V19A1,1,0,0,1,16.977,20Zm0-12a1,1,0,1,0,1,1A1,1,0,0,0,16.977,8Zm-7,12a1,1,0,0,1-1-1V17.82a3,3,0,0,1,0-5.64V1a1,1,0,1,1,2,0V12.18a3,3,0,0,1,0,5.64V19A1,1,0,0,1,9.977,20Zm0-6a1,1,0,1,0,1,1A1,1,0,0,0,9.977,14Zm-7,6a1,1,0,0,1-1-1V9.82a3,3,0,0,1,0-5.64V1a1,1,0,0,1,2,0V4.18a3,3,0,0,1,0,5.64V19A1,1,0,0,1,2.976,20Zm0-14a1,1,0,1,0,1,1A1,1,0,0,0,2.976,6Z" transform="translate(2.024 2)" fill="#333"/>
                    </g>
                </svg>
            </button>
        </div>
    );
};


export default FilterMobileBtn;
