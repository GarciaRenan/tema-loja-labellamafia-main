Estrutura do Projeto

qf-custom - É o app cujos componentes react mais generalistas são criados, ou seja, serve para fazer customizações rápidas.

{nome}-io - É a pasta onde está o tema da loja do storeframework da Vtex, ou seja, onde você vai trabalhar a sua maior parte do tempo.

---------------------------------------------------------------------------------------------------------------------------------------

# PROCESSO DEPLOY VTEX

## Alteração da versão no manifest.json
 caso tenha alterado o loja e o qf-custom, alterar o manifest.json dos dois

## Fazer commit
 Verificar se tem algum pull para ser feito
 -> git push -> git add . -> git commit -m "texto" -> git pull origin main

## Fzer o pull request no bitbucket

## "vtex publish"
 Para cada pasta

## "vtex deploy"
 -> "vtex deploy labellamafia.labellamafia-loja@2.0.31 --force"
 -> "vtex deploy labellamafia.qf-custom@0.0.10 --force"

## "vtex use master"

## "vtex install"
 com o nome do componente alterado
 -> "vtex install labellamagia.qf-custom@0.0.10"
 -> "vtex install labellamafia.labellamafia-loja@2.0.31"