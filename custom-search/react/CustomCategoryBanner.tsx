import React, { useEffect, useState } from 'react'
import { canUseDOM, useRuntime } from 'vtex.render-runtime'
// import { FormattedMessage } from 'react-intl'
import { useCssHandles } from 'vtex.css-handles'

import FadeIn from 'react-fade-in'

// @ts-ignore
import { debounce } from './utils/debounce'
import { Banner, Container } from './CustomCategoryBannerStyle'

const CSS_HANDLES = ['CustomCategoryBanner']

const CustomCategoryBanner: React.FC<CustomCategoryBannerProps> = () => {
  const handles = useCssHandles(CSS_HANDLES)
  const { account } = useRuntime()
  const [imageSrcUrl, setImageSrcUrl] = useState('')
  const [isMobile, setIsMobile] = useState(false)

  if (!canUseDOM) {
    return <></>
  }

  function checkDeviceMediaQuery(callback: Function, fallback: Function) {
    if (window.matchMedia('(max-width: 640px)').matches) {
      callback()

      return
    } else {
      fallback()
    }
  }

  useEffect((): any => {
    const debouncedHandleResize = debounce(function handleResize() {
      checkDeviceMediaQuery(
        () => {
          setIsMobile(true)
          checkImage(getImagePath())
        },
        () => {
          setIsMobile(false)
          checkImage(getImagePath())
        }
      )
    }, 2000)

    window.addEventListener('resize', debouncedHandleResize as any)

    return (_: void) => {
      window.removeEventListener('resize', debouncedHandleResize as any)
    }
  })

  function getImagePath(): string {
    let genericSrc:
      | null
      | string = `https://${account}.vteximg.com.br/arquivos/`
    let imageSrc: null | string = ''

    switch (window?.__RUNTIME__.page) {
      case 'store.search#department':
        let departmentName = window?.__RUNTIME__.route.params.department
        if (departmentName) {
          let imageName: string = `department-${departmentName
            .toLowerCase()
            .replace(' ', '-')}${isMobile ? '--mobile' : ''}.jpg`
          genericSrc = `https://${account}.vteximg.com.br/arquivos/`
          imageSrc = genericSrc + imageName
          checkImage(imageSrc)
          break
        }
        break
      case 'store.search#category':
        departmentName = window?.__RUNTIME__.route.params.department
        if (departmentName) {
          let imageName: string = `department-${departmentName
            .toLowerCase()
            .replace(' ', '-')}${isMobile ? '--mobile' : ''}.jpg`
          genericSrc = `https://${account}.vteximg.com.br/arquivos/`
          imageSrc = genericSrc + imageName
          checkImage(imageSrc)
        }
        setTimeout(() => {
          let categoryName = window?.__RUNTIME__.route.params.category
          if (categoryName) {
            let imageName: string = `category-${categoryName
              .toLowerCase()
              .replace(' ', '-')}${isMobile ? '--mobile' : ''}.jpg`
            genericSrc = `https://${account}.vteximg.com.br/arquivos/`
            imageSrc = genericSrc + imageName
            checkImage(imageSrc)
          }
        }, 1000)
        break
      case 'store.search#subcategory':
        let imageName: string = `default-search-banner${
          isMobile ? '--mobile' : ''
        }.jpg`
        genericSrc = `https://${account}.vteximg.com.br/arquivos/`
        imageSrc = genericSrc + imageName
        checkImage(imageSrc)
        setTimeout(() => {
          departmentName = window?.__RUNTIME__.route.params.departmentName
          if (departmentName) {
            let imageName: string = `department-${departmentName
              .toLowerCase()
              .replace(' ', '-')}${isMobile ? '--mobile' : ''}.jpg`
            genericSrc = `https://${account}.vteximg.com.br/arquivos/`
            imageSrc = genericSrc + imageName
            checkImage(imageSrc)
          }
        }, 1000)
        break
      default:
        imageName = `default-search-banner${isMobile ? '--mobile' : ''}.jpg`
        genericSrc = `https://${account}.vteximg.com.br/arquivos/`
        imageSrc = genericSrc + imageName
        checkImage(imageSrc)
    }
    return imageSrc
  }

  function checkImage(url: string) {
    var request = new XMLHttpRequest()
    request.open('GET', url, true)
    request.send()
    request.onload = function () {
      if (request.status == 200) {
        console.warn('banner exists')
        setImageSrcUrl(url)
      } else {
        console.warn("banner doesn't exist")
      }
    }
    request.onerror = function () {
      console.warn('banner path throws error')
    }
    return
  }

  useEffect(() => {
    checkDeviceMediaQuery(
      () => {
        if (!isMobile) setIsMobile(true)
      },
      () => {
        if (isMobile) {
          setIsMobile(false)
        }
      }
    )
  }, [])

  useEffect(() => {
    checkImage(getImagePath())
  }, [isMobile])

  return (
    <>
      <FadeIn delay={1000}>
        <Container className={`${handles.CustomCategoryBannerModalImage}`}>
          <Banner src={imageSrcUrl} alt="" />
        </Container>
      </FadeIn>
    </>
  )
}

interface CustomCategoryBannerProps {}

export default CustomCategoryBanner
