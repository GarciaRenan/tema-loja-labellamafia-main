import React from 'react'
import { useSearchPage } from 'vtex.search-page-context/SearchPageContext'
import { useCssHandles } from 'vtex.css-handles'

import OrderBy from './OrderBy'

import styles from './searchResult.css'

const CSS_HANDLES = ['orderByButton']

const withSearchPageContextProps = (Component) => (props) => {
  const { orderBy } = useSearchPage()
  const handles = useCssHandles(CSS_HANDLES)
  if (orderBy == null) {
    return null
  }

  return (
    <div className={styles['orderBy--layout']}>
      <Component
        {...props}
        orderBy={orderBy}
        className={handles.orderByButton}
      />
    </div>
  )
}

const OrderByFlexible = withSearchPageContextProps(OrderBy)

OrderByFlexible.schema = {
  title: 'admin/editor.search-result.ordination.sort-by',
}

export default OrderByFlexible
