import React, { useMemo } from 'react'
import { flatten } from 'ramda'
import { useSearchPage } from 'vtex.search-page-context/SearchPageContext'
import { useDevice } from 'vtex.device-detector'

import FilterNavigator from './FilterNavigator'
import FilterNavigatorContext from './components/FilterNavigatorContext'
import useFacetNavigation, {
  buildNewQueryMap,
} from './hooks/useFacetNavigation'

import { newFacetPathName } from './utils/slug'

import styles from './searchResult.css'

const getSelectedCategories = (tree) => {
  for (const node of tree) {
    if (!node.selected) {
      continue
    }
    if (node.children) {
      return [node, ...getSelectedCategories(node.children)]
    } else {
      return [node]
    }
  }
  return []
}

const newNamedFacet = (facet) => {
  return { ...facet, newQuerySegment: newFacetPathName(facet) }
}

const withSearchPageContextProps =
  (Component) =>
  ({
    layout,
    initiallyCollapsed,
    scrollToTop,
    maxItemsDepartment,
    maxItemsCategory,
    categoryFiltersMode,
    filtersTitleHtmlTag,
    truncateFilters,
    openFiltersMode,
    closeOnOutsideClick,
    appliedFiltersOverview,
    totalProductsOnMobile,
    fullWidthOnMobile,
    navigationTypeOnMobile,
    colorSpecificationName,
  }) => {
    const {
      searchQuery,
      map,
      params,
      priceRange,
      hiddenFacets,
      filters,
      showFacets,
      preventRouteChange,
      facetsLoading,
    } = useSearchPage()
    const { isMobile } = useDevice()

    const filtersHidden = hiddenFacets.specificationFilters.hiddenFilters
      ? hiddenFacets.specificationFilters.hiddenFilters
      : filters

    const filtersVisible = filters.filter((el) => {
      if (!filtersHidden.includes(el.title)) return el
    })

    const filtersFetchMore =
      searchQuery && searchQuery.facets && searchQuery.facets.facetsFetchMore
        ? searchQuery.facets.facetsFetchMore
        : undefined

    const facets =
      searchQuery && searchQuery.data && searchQuery.data.facets
        ? searchQuery.data.facets
        : {}

    const {
      brands,
      priceRanges,
      specificationFilters,
      categoriesTrees,
      queryArgs,
    } = facets

    const { recordsFiltered } = searchQuery

    if (showFacets === false || !map) {
      return null
    }

    const selectedFilters = useMemo(() => {
      const options = [
        ...specificationFilters.map((filter) => {
          return filter.facets.map((facet) => {
            return {
              ...newNamedFacet({ ...facet, title: filter.name }),
              hidden: filter.hidden,
            }
          })
        }),
        ...brands,
        ...priceRanges,
      ]
      return flatten(options)
    }, [brands, priceRanges, specificationFilters]).filter(
      (facet) => facet.selected
    )

    const selectedCategories = getSelectedCategories(categoriesTrees)
    const navigateToFacet = useFacetNavigation(
      useMemo(() => {
        return selectedCategories.concat(selectedFilters)
      }, [selectedFilters, selectedCategories]),
      scrollToTop
    )

    const handleClearFilters = () => {
      navigateToFacet(selectedFilters)
    }

    const handleCloseModal = () => {
      const filterMobile = document.querySelector(
        '.vtex-flex-layout-0-x-flexColChild--filterMobileCol .labellamafia-custom-search-result-0-x-filters--layout'
      )
      const overlayMobile = document.querySelector(
        '.vtex-flex-layout-0-x-flexColChild--filterMobileCol .labellamafia-custom-search-result-0-x-queryOverlay'
      )

      if (filterMobile) {
        filterMobile.classList.remove('open')
        overlayMobile?.classList.remove('open')
      }
    }

    return (
      <>
        {isMobile && (
          <div onClick={handleCloseModal} className={styles.queryOverlay}></div>
        )}
        <div
          className={`${styles['filters--layout']} ${
            layout === 'desktop' && isMobile ? 'w-100 mh5' : ''
          }`}
        >
          {searchQuery.data?.searchMetadata?.titleTag && !isMobile && (
            <p className={styles.queryTitle}>
              {searchQuery.data?.searchMetadata?.titleTag}

              <span className={styles.queryQuantity}>[{recordsFiltered}]</span>
            </p>
          )}
          {isMobile && (
            <>
              <p className={styles.queryTitle}>
                Filtrar por
                <span
                  onClick={handleClearFilters}
                  className={`${styles.queryQuantity} clear-filter ${styles.closeModal}`}
                >
                  limpar
                </span>
                /
                <span
                  className={`${styles.queryQuantity} close-modal ${styles.closeModal}`}
                  onClick={handleCloseModal}
                >
                  <svg
                    id="Icons_User_Interface_multiply"
                    data-name="Icons/User Interface/multiply"
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                  >
                    <path
                      id="multiply"
                      d="M9.416,8l6.3-6.29A1,1,0,0,0,14.3.294l-6.29,6.3L1.716.294A1,1,0,0,0,.3,1.714L6.6,8,.3,14.294a1,1,0,1,0,1.42,1.42l6.29-6.3,6.29,6.3a1,1,0,1,0,1.42-1.42Z"
                      transform="translate(3.994 3.996)"
                      fill="#333"
                    />
                  </svg>
                </span>
              </p>
            </>
          )}

          <FilterNavigatorContext.Provider value={queryArgs}>
            <Component
              preventRouteChange={preventRouteChange}
              brands={brands}
              params={params}
              priceRange={priceRange}
              priceRanges={priceRanges}
              specificationFilters={specificationFilters}
              tree={categoriesTrees}
              loading={facetsLoading}
              filters={filtersVisible}
              filtersFetchMore={filtersFetchMore}
              hiddenFacets={hiddenFacets}
              layout={layout}
              initiallyCollapsed={initiallyCollapsed}
              scrollToTop={scrollToTop}
              maxItemsDepartment={maxItemsDepartment}
              maxItemsCategory={maxItemsCategory}
              categoryFiltersMode={categoryFiltersMode}
              filtersTitleHtmlTag={filtersTitleHtmlTag}
              truncateFilters={truncateFilters}
              openFiltersMode={openFiltersMode}
              closeOnOutsideClick={closeOnOutsideClick}
              appliedFiltersOverview={appliedFiltersOverview}
              totalProductsOnMobile={totalProductsOnMobile}
              fullWidthOnMobile={fullWidthOnMobile}
              navigationTypeOnMobile={navigationTypeOnMobile}
              colorSpecificationName={colorSpecificationName}
            />
          </FilterNavigatorContext.Provider>
        </div>
      </>
    )
  }

export default withSearchPageContextProps(FilterNavigator)
