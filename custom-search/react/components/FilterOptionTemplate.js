import PropTypes from 'prop-types'
import React, {
  useState,
  useCallback,
  useMemo,
  useRef,
  useEffect,
  useContext,
} from 'react'
import { Collapse } from 'react-collapse'
import classNames from 'classnames'

import { useRuntime } from 'vtex.render-runtime'
import { IconCaret } from 'vtex.store-icons'
import { useCssHandles } from 'vtex.css-handles'

import styles from '../searchResult.css'
import { SearchFilterBar } from './SearchFilterBar'
import SettingsContext from './SettingsContext'
import useOutsideClick from './../hooks/useOutsideClick'
import ShowMoreFilterButton from './ShowMoreFilterButton'

import { useRenderOnView } from '../hooks/useRenderOnView'
import { FACETS_RENDER_THRESHOLD } from '../constants/filterConstants'

/** Returns true if elementRef has ever been scrolled */
const useHasScrolled = (elementRef) => {
  const [hasScrolled, setHasScrolled] = useState(false)

  useEffect(() => {
    const scrollableElement = elementRef.current
    if (hasScrolled || !scrollableElement) {
      return
    }

    const handleScroll = () => {
      setHasScrolled(true)
    }

    scrollableElement.addEventListener('scroll', handleScroll)

    return () => {
      scrollableElement.removeEventListener('scroll', handleScroll)
    }
  }, [hasScrolled, elementRef])

  return hasScrolled
}

const CSS_HANDLES = [
  'filter__container',
  'filter',
  'filterSelected',
  'filterAvailable',
  'filterIsOpen',
  'filterTitle',
  'filterIcon',
  'filterContent',
  'filterTemplateOverflow',
  'seeMoreButton',
  'filterSelectedFilters',
  'filterVisible',
  'filterHide',
]

const useSettings = () => useContext(SettingsContext)
/**
 * Collapsable filters container
 */
const FilterOptionTemplate = ({
  id,
  selected = false,
  title,
  quantity,
  collapsable = true,
  children,
  filters,
  initiallyCollapsed = false,
  lazyRender = false,
  truncateFilters = false,
  lastOpenFilter,
  setLastOpenFilter,
  openFiltersMode,
  truncatedFacetsFetched,
  setTruncatedFacetsFetched,
  closeOnOutsideClick = false,
  appliedFiltersOverview,
}) => {
  const [open, setOpen] = useState(!initiallyCollapsed)
  const { getSettings } = useRuntime()
  const scrollable = useRef()
  const filterRef = useRef()
  const handles = useCssHandles(CSS_HANDLES)
  const { thresholdForFacetSearch } = useSettings()
  const [searchTerm, setSearchTerm] = useState('')
  const [truncated, setTruncated] = useState(true)
  const isMobile = window?.innerWidth < 1020
  const [filterOptionStatus, setFilterOptionStatus] = useState(!isMobile)

  const isLazyRenderEnabled =
    getSettings('vtex.store')?.enableSearchRenderingOptimization
  const isLazyFacetsFetchEnabled =
    getSettings('vtex.store')?.enableFiltersFetchOptimization

  const { hasBeenViewed, dummyElement } = useRenderOnView({
    lazyRender: isLazyRenderEnabled && lazyRender,
    waitForUserInteraction: false,
  })

  const hasScrolled = useHasScrolled(scrollable)
  const isOpen = openFiltersMode === 'many' ? open : lastOpenFilter === title

  const filteredFacets = useMemo(() => {
    if (thresholdForFacetSearch === undefined || searchTerm === '') {
      return filters
    }
    return filters.filter(
      (filter) =>
        filter.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
    )
  }, [filters, searchTerm, thresholdForFacetSearch])

  const openTruncated = (value) => {
    if (isLazyFacetsFetchEnabled && !truncatedFacetsFetched) {
      setTruncatedFacetsFetched(true)
    }
    setTruncated(value)
  }

  const renderChildren = () => {
    if (typeof children !== 'function') {
      return children
    }

    const shouldTruncate =
      (truncateFilters || isLazyFacetsFetchEnabled) &&
      quantity > FACETS_RENDER_THRESHOLD + 1

    const shouldLazyRender =
      !shouldTruncate && !hasScrolled && isLazyRenderEnabled

    /** Inexact measure but good enough for displaying a properly sized scrollbar */
    const placeholderSize = shouldLazyRender
      ? (filters.length - FACETS_RENDER_THRESHOLD) * 34
      : 0

    const endSlice =
      shouldLazyRender || (shouldTruncate && truncated)
        ? FACETS_RENDER_THRESHOLD
        : filteredFacets.length

    return (
      <>
        {filteredFacets.slice(0, endSlice).map(children)}
        {placeholderSize > 0 && <div style={{ height: placeholderSize }} />}
        {shouldTruncate && (
          <ShowMoreFilterButton
            quantity={quantity - FACETS_RENDER_THRESHOLD}
            truncated={truncated}
            toggleTruncate={() => openTruncated((truncated) => !truncated)}
          />
        )}
      </>
    )
  }

  const handleCollapse = useCallback(() => {
    if (openFiltersMode === 'many') {
      setOpen(!open)
    } else if (openFiltersMode === 'one') {
      setLastOpenFilter(lastOpenFilter === title ? null : title)
    } else {
      console.error(
        `Invalid openFiltersMode value: ${openFiltersMode}\nCheck the documentation for the values available`
      )
    }
  }, [lastOpenFilter, open, openFiltersMode, setLastOpenFilter, title])

  useOutsideClick(
    filterRef,
    () => {
      // closeOnOutsideClick only works with openFiltersMode == 'one'
      if (closeOnOutsideClick && openFiltersMode === 'one') {
        handleCollapse()
      }
    },
    isOpen
  )

  const handleKeyDown = useCallback(
    (e) => {
      if (e.key === ' ' && collapsable) {
        e.preventDefault()
        handleCollapse()
      }
    },
    [collapsable, handleCollapse]
  )

  const containerClassName = classNames(
    handles.filter__container,
    { [`${styles['filter__container']}--${id}`]: id },
    'bb b--muted-4'
  )

  const filterContentClassName = classNames(
    handles.filterContent,
    handles.filterContent + '--' + String(title).toLowerCase()
  )

  const titleContainerClassName = classNames(handles.filter, 'pv5', {
    [handles.filterSelected]: selected,
    [handles.filterAvailable]: !selected,
    [handles.filterIsOpen]: isOpen,
  })

  const titleClassName = classNames(
    handles.filterTitle,
    'f5 flex items-center justify-between',
    {
      ttu: selected,
    }
  )

  const handleClickOptionTemplate = () => {
    console.log('a')
    setFilterOptionStatus(!filterOptionStatus)
  }

  return (
    <div className={containerClassName} ref={filterRef}>
      <div className={titleContainerClassName}>
        <div
          className={titleClassName}
          onClick={() => handleClickOptionTemplate()}
        >
          {title}
        </div>
        {appliedFiltersOverview === 'show' && filters && !selected && (
          <div className={classNames(handles.filterSelectedFilters, 'f6')}>
            {filters
              .filter((facet) => facet.selected)
              .map((facet) => facet.name)
              .join(', ')}
          </div>
        )}
        {window?.innerWidth < 1020 && (
          <svg
            id="Icons_Arrows_angle-right-b"
            data-name="Icons/Arrows/angle-right-b"
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
          >
            <path
              id="angle-right-b"
              d="M7.371,5.946,1.711.3a1,1,0,1,0-1.42,1.41l4.95,5-4.95,4.95a1,1,0,1,0,1.42,1.41l5.66-5.65a1,1,0,0,0,0-1.47Z"
              transform="translate(8.169 5.344)"
              fill="#333"
            />
          </svg>
        )}

        {window?.innerWidth > 1020 && (
          <svg
            className={styles.arrowTitle}
            xmlns="http://www.w3.org/2000/svg"
            width="10.542"
            height="6.247"
            viewBox="0 0 10.542 6.247"
          >
            <path
              id="angle-down"
              d="M10.246.291a1,1,0,0,0-1.41,0l-3.59,3.54L1.706.291A1,1,0,1,0,.3,1.711l4.24,4.24a1,1,0,0,0,1.42,0l4.29-4.24a1,1,0,0,0,0-1.42Z"
              fill="#333"
            />
          </svg>
        )}
      </div>
      <div
        className={`${handles.filterTemplateOverflow} ${
          handles.filterTemplateOverflow
        }--${id.toLowerCase()} ${
          filterOptionStatus ? handles.filterVisible : handles.filterHide
        }`}
        ref={scrollable}
      >
        {renderChildren()}
      </div>
    </div>
  )
}

FilterOptionTemplate.propTypes = {
  /** Identifier to be used by CSS handles */
  id: PropTypes.string,
  /** Filters to be shown, if no filter is provided, treat the children as simple node */
  filters: PropTypes.arrayOf(PropTypes.object),
  /** Function to handle filter rendering or node if no filter is provided */
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.node]).isRequired,
  /** Title */
  title: PropTypes.node,
  /** Whether collapsing is enabled */
  collapsable: PropTypes.bool,
  /** Whether it represents the selected filters */
  selected: PropTypes.bool,
  initiallyCollapsed: PropTypes.bool,
  /** Internal prop, whether this component should be rendered only on view */
  lazyRender: PropTypes.bool,
  /** When `true`, truncates filters with more than 10 options displaying a button to see all */
  truncateFilters: PropTypes.bool,
  /** Last open filter */
  lastOpenFilter: PropTypes.string,
  /** Sets the last open filter */
  setLastOpenFilter: PropTypes.func,
  /** Dictates how many filters can be open at the same time */
  openFiltersMode: PropTypes.string,
  /** If the truncated facets were fetched */
  truncatedFacetsFetched: PropTypes.bool,
  /** Sets if the truncated facets were fetched */
  setTruncatedFacetsFetched: PropTypes.func,
  /** Quantity of facets of the current filter */
  quantity: PropTypes.number,
  closeOnOutsideClick: PropTypes.bool,
  /** Whether an overview of the applied filters should be displayed (`"show"`) or not (`"hide"`). */
  appliedFiltersOverview: PropTypes.string,
}

export default FilterOptionTemplate
