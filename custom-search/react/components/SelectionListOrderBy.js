import React, { useState, useCallback, useRef } from 'react'
import PropTypes from 'prop-types'
import { injectIntl, intlShape } from 'react-intl'
import classNames from 'classnames'
import { find, propEq } from 'ramda'
import { formatIOMessage } from 'vtex.native-types'
import { IconCaret } from 'vtex.store-icons'
import { useDevice } from 'vtex.device-detector'
import { useCssHandles } from 'vtex.css-handles'

import SelectionListItem from './SelectionListItem'
import useOutsideClick from '../hooks/useOutsideClick'
import styles from '../searchResult.css'

const CSS_HANDLES = [
  'orderByButton',
  'arrowIconOpen',
  'orderByOptionsContainer',
  'orderByDropdown',
  'orderByText',
  'filterPopupTitle',
  'filterPopupArrowIcon',
]

const SelectionListOrderBy = ({
  intl,
  message = 'store/ordenation.sort-by',
  orderBy,
  options,
  showOrderTitle,
}) => {
  const [showDropdown, setShowDropdown] = useState(false)
  const handles = useCssHandles(CSS_HANDLES)

  const orderByRef = useRef(null)

  const handleDropdownBtClick = useCallback(
    () => setShowDropdown(!showDropdown),
    [showDropdown]
  )

  const handleOutsideClick = useCallback(() => setShowDropdown(false), [])

  useOutsideClick(orderByRef, handleOutsideClick, showDropdown)

  const { isMobile } = useDevice()

  const renderOptions = (orderBy) => {
    return options.map((option) => {
      return (
        <SelectionListItem
          key={option.value}
          onItemClick={handleOutsideClick}
          option={option}
          selected={option.value === orderBy}
        />
      )
    })
  }

  const sortByMessage = formatIOMessage({ id: message, intl })

  const getOptionTitle = useCallback(
    (option) => {
      const selectedOption = find(propEq('value', option), options)
      return selectedOption ? selectedOption.label : ''
    },
    [options]
  )

  const btClass = classNames(
    handles.orderByButton,
    'ph3 pv5 mv0 pointer flex items-center justify-end bg-base c-on-base t-action--small bt br bl bb-0 br2 br--top bw1 w-100 outline-0',
    {
      'b--muted-4': showDropdown && isMobile,
      'b--transparent pl1': !showDropdown,
    }
  )

  const contentClass = classNames(
    styles.orderByOptionsContainer,
    'z-1 absolute bg-base shadow-5 w-100 f5 b--muted-4 br2 ba bw1 br--bottom top-0 right-0-ns',
    {
      db: showDropdown,
      dn: !showDropdown,
    }
  )

  const dropdownSort = classNames(
    handles.orderByDropdown,
    'relative pt1 justify-end w-100 w-auto-ns ml-auto'
  )

  return (
    <div className={dropdownSort} ref={orderByRef}>
      <button onClick={handleDropdownBtClick} className={btClass}>
        <span
          className={classNames(
            handles.filterPopupTitle,
            'c-on-base t-action--small ml-auto-ns'
          )}
        >
          <span
            className={classNames(handles.orderByText, 'c-muted-2', {
              'dn dib-ns': !orderBy.length,
            })}
          >
            Ordenar
          </span>
          {showOrderTitle ? `Por ${getOptionTitle(orderBy)}` : null}
        </span>
        <span
          className={`${handles.filterPopupArrowIcon} ${
            showDropdown ? handles.arrowIconOpen : ''
          } ph5 pt1`}
        >
          {window?.innerWidth > 1020 && (
            <IconCaret orientation={showDropdown ? 'up' : 'down'} size={10} />
          )}
          {window?.innerWidth < 1020 && (
            <svg
              id="Icons_Arrows_angle-right-b"
              data-name="Icons/Arrows/angle-right-b"
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
            >
              <path
                id="angle-right-b"
                d="M7.371,5.946,1.711.3a1,1,0,1,0-1.42,1.41l4.95,5-4.95,4.95a1,1,0,1,0,1.42,1.41l5.66-5.65a1,1,0,0,0,0-1.47Z"
                transform="translate(8.169 5.344)"
                fill="#333"
              />
            </svg>
          )}
        </span>
      </button>

      <div className={contentClass}>{renderOptions(orderBy)}</div>
    </div>
  )
}

SelectionListOrderBy.propTypes = {
  /** Current Ordernation  */
  orderBy: PropTypes.string,
  /** Sort Options*/
  options: PropTypes.arrayOf(
    PropTypes.shape({
      /** Label to Option */
      label: PropTypes.string,
      /** Value to value */
      value: PropTypes.string,
    })
  ),
  /** Intl to translations */
  intl: intlShape,
  /** Message to be displayed */
  message: PropTypes.string,
  /** Show or hide order title */
  showOrderTitle: PropTypes.boolean,
}

export default injectIntl(SelectionListOrderBy)
